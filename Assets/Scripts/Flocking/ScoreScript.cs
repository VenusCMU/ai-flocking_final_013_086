﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScoreScript : MonoBehaviour
{
    public Text ScoreText;
    public static int ScoreVaule;
   public void Start()
    {
        ScoreText = GetComponent<Text>();
    }

    public void Update()
    {
        //ScoreText.text = " : " + ScoreVaule;
        ScoreText.text = GameManagerSingleton.Instance.Score.ToString();
    }
}
