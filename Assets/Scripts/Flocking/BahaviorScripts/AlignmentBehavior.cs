﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Flock/Behavior/Alignment")]
public class AlignmentBehavior : FilterFlockBehavior
{
    public override Vector2 CalculateMove(FlockAgent agent, List<Transform> context, Flock flock)
    {
        Transform Player = GameObject.FindWithTag("Player").transform;

        //if no neighbor, maintain current alignment
        if (context.Count == 0)
            return agent.transform.up;

        //add all points together and average
        Vector2 alignmentMove = Vector2.zero;
        List<Transform> filteredContext = (filter == null) ? context : filter.Filter(agent, context);
        foreach (Transform item in filteredContext)
        {
            alignmentMove += (Vector2) item.transform.up;
        }

        alignmentMove /= context.Count;
        
        if (Player.localScale.x >= agent.transform.localScale.x)
        {
            return alignmentMove;
        }
        return alignmentMove/2;
    }
}

