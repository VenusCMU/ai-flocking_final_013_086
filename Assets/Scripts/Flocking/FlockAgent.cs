﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(Collider2D))]
public class FlockAgent : MonoBehaviour
{
    Flock agentFlock;
    public Flock AgentFlock {get{ return agentFlock;}}
    Collider2D agentCollider;

    public Collider2D AgentCollider
    {
        get { return agentCollider; }
    }
    
    void Start()
    {
        agentCollider = GetComponent<Collider2D>();
    }

    public void Initialize(Flock flock)
    {
        agentFlock = flock;
        
    }

    public void Move(Vector2 velocity)
    {
        transform.up = velocity;
        transform.position += (Vector3)velocity * Time.deltaTime;
    }

    public void Update()
    {
        Transform Player = GameObject.FindWithTag("Player").transform;
        Color c = this.GetComponentInChildren<SpriteRenderer>().color;
        if (gameObject.transform.localScale.x < Player.localScale.x)
        {
            c = new Color(c.r, c.g, c.b,
                gameObject.transform.localScale.x / Player.localScale.x);
            this.GetComponentInChildren<SpriteRenderer>().color = c;
        }
        else
        {
            c = new Color(c.r, c.g, c.b, 1);
            this.GetComponentInChildren<SpriteRenderer>().color = c;
        }
        if (gameObject.transform.localScale.x < Player.localScale.x * 0.3f)
        {
            gameObject.transform.localScale = Player.localScale*Random.Range(.5f,3f);
            if (Random.Range(0f, 20f) >= 19)
            {
                gameObject.transform.localScale *= 5;
                Debug.Log("The Big has spawn");
            }
            transform.position = Player.GetComponent<PlayerController>().randomDonut;
        }

        if (Mathf.Abs((gameObject.transform.position - Player.position).magnitude) > 5 * Player.localScale.x)
        {
            transform.position = Player.GetComponent<PlayerController>().randomDonut;
        }
    }
}
