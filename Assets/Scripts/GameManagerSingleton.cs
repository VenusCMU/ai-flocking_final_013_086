﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class GameManagerSingleton : MonoBehaviour {
    private static GameManagerSingleton _instance;
    public static GameManagerSingleton Instance { get { return _instance; } }

    public ushort GameState = 0; // 0 = Alive, 1 = Pause, 2 = Dead, 3 = Win
    public ushort Score = 0;
    private Text LoseText;
    private float Delay = 3;
    

    private void Awake()
    {
        if (_instance != null && _instance != this)
        {
            Destroy(this.gameObject);
        } else {
            _instance = this;
        }
    }

    public void Update()
    {
        if(LoseText == null)
        {
            LoseText = GameObject.Find("EndGame").GetComponent<Text>();
        }
        switch (GameState) 
        {
            case 0 :
            Time.timeScale =1;
            break;
            case 1 : 
            Time.timeScale = 0;
            break;
            case 2 :
            LoseText.enabled= true;
            LoseText.text = "GameOver\n" + Score;
            Delay -= Time.deltaTime;
            if(Delay <= 0)
            {
                GameState = 0;
                Score = 0;
                Delay =3;
                LoseText =null;
                SceneManager.LoadScene("maingame");
            }
            break;


        }
        
    }

    
}
